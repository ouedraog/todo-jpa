package bf.isge.ic3.todoapi.service.jpa;

import bf.isge.ic3.todoapi.dto.CreateTodoDto;
import bf.isge.ic3.todoapi.dto.UpdateTodoDto;
import bf.isge.ic3.todoapi.exception.TodoNotFoundException;
import bf.isge.ic3.todoapi.model.Todo;
import bf.isge.ic3.todoapi.model.User;
import bf.isge.ic3.todoapi.repository.TodoRepository;
import bf.isge.ic3.todoapi.service.TodoService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
@Service
public class TodoJpaServiceImpl implements TodoService {
    private TodoRepository todoRepository;
    public TodoJpaServiceImpl(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }
    @Override
    public Optional<Todo> findById(int id) {
        return todoRepository.findById(id);
    }

    @Override
    public Todo createTodo(CreateTodoDto todoDto) {
        Todo todo = new Todo();
        todo.setTitle(todoDto.getTitle());
        todo.setDescription(todoDto.getDescription());
        User user = new User();
        user.setId(todoDto.getAuthorId());
        todo.setAuthor(user);
        return todoRepository.save(todo);
    }

    @Override
    public Todo updateTodo(int id, UpdateTodoDto todoDto) throws TodoNotFoundException {
        Todo todo = todoRepository.findById(id).orElseThrow();
        todo.setTitle(todoDto.getTitle());
        todo.setDescription(todoDto.getDescription());
        return todoRepository.save(todo);
    }

    @Override
    public boolean removeTodo(int id) {
        todoRepository.deleteById(id);
        return true;
    }

    @Override
    public List<Todo> findAll() {
        List<Todo> result = new ArrayList<>();
        for(Todo todo: todoRepository.findAll()) {
            result.add(todo);
        }
        return result;
    }
}
