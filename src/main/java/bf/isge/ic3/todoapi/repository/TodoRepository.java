package bf.isge.ic3.todoapi.repository;

import bf.isge.ic3.todoapi.model.Todo;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TodoRepository extends PagingAndSortingRepository<Todo, Integer> {
}
