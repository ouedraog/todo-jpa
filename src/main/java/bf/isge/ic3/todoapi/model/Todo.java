package bf.isge.ic3.todoapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
public class Todo {
    @Id
    @GeneratedValue
    private int id;
    private String title;
    private String description;
    @ManyToOne()
    private User author;
    private LocalDateTime createdAt;
    @Column(columnDefinition = "boolean default false")
    private boolean archived;

    @PrePersist
    public void setup() {
        this.createdAt = LocalDateTime.now();
    }
}
