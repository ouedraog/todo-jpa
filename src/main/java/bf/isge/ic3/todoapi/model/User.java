package bf.isge.ic3.todoapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity(name = "users")
public class User {
    @Id
    @GeneratedValue
    private int id;
    private String name;
    @JsonIgnore
    @OneToMany(mappedBy = "author")
    private List<Todo> todoList = new ArrayList<>();
}
